package model;

public class FormatException extends Exception {
	public FormatException() {
		super();
	}
	public FormatException(String message) {
		super(message);
	}
}
