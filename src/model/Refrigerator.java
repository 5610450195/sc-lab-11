package model;

import java.util.ArrayList;

import model.FullException;

public class Refrigerator {
	private int size;
	private ArrayList<String> things = new ArrayList<String>();
	
	public Refrigerator(int size){
		this.size = size;
	}
	
	public void put(String stuff) throws FullException{
		if(things.size() >= size) throw new FullException("Error Full Fridge");
		else things.add(stuff);
	}
	
	public String takeOut(String stuff){
		for(int i = 0 ; i < things.size() ; i++){
			if(things.get(i).equals(stuff)){
				things.remove(i);
				return stuff;
			}
		}
		return null;
	}
	
	public String toString(){
		return things.toString();
	}
}
