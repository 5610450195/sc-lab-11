package model;
import model.DataException;
public class MyClass {
	/* Error in throw DataException() because It doesn't has "new" 
	 * and must declare after method name with "throws DataException()"
	 * Error in throws new FormatException() because throw has "s"
	 */
	
	public void methX() throws DataException {
		throw new DataException();
	}
	
	public void methY() throws FormatException {
		throw new FormatException();
	}
	public static void main(String[] args) {
		try{
			MyClass c = new MyClass();
			System.out.print("A");
			c.methX();
			System.out.print("B");
			c.methY();
			System.out.print("C");
			return;
		}
		
		catch(DataException e){
			//System.out.println("D");
		}
		
		catch(FormatException e){
			System.out.print("E");
		}
		
		finally{
			System.out.print("F");
		}
		
		System.out.print("G");

	}
}
