package model;
import java.util.Collection;
import java.util.HashMap;


public class WordCounter {
	private String message;
	private HashMap<String,Integer> wordCount = new HashMap<String,Integer>();
	
	public WordCounter(String message){
		this.message = message;
	}
	
	public void count() {
		String[] m = message.split(" ");
		for (String num : m) {
			if (wordCount.keySet().contains(num)) wordCount.put(num, wordCount.get(num)+1);
			else wordCount.put(num, 1);
		}
	}
	
	public int hasWord(String word) {
		if (wordCount.get(word) == null) return 0;
		else return wordCount.get(word);
		
	}
}
