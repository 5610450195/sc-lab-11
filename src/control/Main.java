package control;

import model.FullException;
import model.Refrigerator;

public class Main {
	public static void main (String args[]) {
		try {
			Refrigerator r1 = new Refrigerator(2);
			r1.put("Cake");
			r1.put("Beer");
			System.out.println(r1.takeOut("Cake"));
			System.out.println(r1.takeOut("Onion"));
		}
		catch(FullException e) {
			System.out.println("Error Full Fridge");
		}
		try {
			Refrigerator r2 = new Refrigerator(2);
			r2.put("Cake");
			r2.put("Beer");
			r2.put("Eggs");
			System.out.println(r2.takeOut("Cake"));
		}
		catch(FullException e) {
			System.out.println("Error The fridge is already full");
		}
	}
}
